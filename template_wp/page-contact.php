<?php get_header(); ?>

<div class="page">
    <div class="col-sm-12">
        <div class="container">
            <div class="col-sm-8 col-sm-offset-2">

                <h1>Contact</h1>

                <?php
                if(have_posts())
                {
                    while(have_posts())
                    {
                        the_post();
                        the_title();
                        the_content();
                    }
                }
                else
                {
                    echo 'No content available';
                }
                ?>

                <?php get_sidebar(); ?>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>


