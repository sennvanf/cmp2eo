<?php get_header(); ?>

<div class="page">
    <div class="col-sm-12">
        <div class="container">
            <div class="col-sm-2"></div>
            <div class="col-sm-8 text-center">
                <?php
                if(have_posts())
                {
                    while(have_posts())
                    {
                        the_post();
                        the_title();
                        the_content();
                    }
                }
                else
                {
                    echo 'No content available';
                }
                ?>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>

