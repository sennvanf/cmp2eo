<?php
/**
 * Created by PhpStorm.
 * User: sennevanfleteren
 * Date: 26/02/16
 * Time: 09:08
 */

class Database {

    private $_db_host = 'localhost';
    private $_db_port = '3306';
    private $_db_name = 'employees';
    private $_db_user = 'root';
    private $_db_password = 'bitnami';
    private $_db_charset = 'utf8';

    private $_db = null;

    public function connect (){
        /**
         * Configuratie voor MySQL Server.
         */

        $dsn_properties = [
            'dbname'  => $this->_db_name,   // Naam van het Database Schema (moet al bestaan in MySQL).
            'host'    => $this->_db_host,
            'port'    =>  $this->_db_port,       // 3306 is de standaardpoort voor MySQL.
            'charset' => $this->_db_charset,      // Zorgt ervoor dat de verbinding tussen PHP en MySQL in UTF-8-tekencodering gebeurt.
        ];

        // Data Source Name samestellen.
        $dsn = 'mysql:';
        foreach ($dsn_properties as $property => $value) {
            $dsn .= "{$property}={$value};";
        }

        // Foutmeldingen
        error_reporting(0); // Standaardfoutmelding afzetten.
        try {               // Eigen foutmelding.
            /**
             * Verbinding maken met de database via een nieuwe PDO object.
             */
            $this->_db = new PDO(
                $dsn, // Data Source Name
                $this->_db_user,
                $this->_db_password,
                null);
        var_dump('Databaseverbinding met MySQL geopend.');
        } catch (PDOException $e) {
            die('Databaseverbinding mislukt: <pre>' . $e->getMessage() .'</pre>');
        }

        /**
         * Alle foutmeldingen weer aanzetten, inclusief die van PDO.
         * Gebruik deze opties NOOIT op een productieserver, want dit is een schat aan informatie voor hackers!
         */
        error_reporting(E_ALL);
        $this->_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
    }

    public function disconnect(){

        $db=null;
        var_dump('Databaseverbinding met MySql wordt gesloten');

    }

    public function select($table){
        //tabel
        //select all => *
        //select array van velden...


        //1e oef. select all (*) van tabel met 1 argument...
        $sql_select = 'SELECT * FROM ' .$table. ' LIMIT 10';
        var_dump("Read-statement (CRUD): {$sql_select}");

        //Resultaat(result set) van de query opvragen via het databaseconnection
        $result_select = $this->_db->query($sql_select);
        if ($result_select) {
            //Alle rijen in één keer opvragen.
            if ($rows_select = $result_select->fetchAll()){
                var_dump('Records in' . $table . ':', $rows_select);
            }
        }
    }

    public function insert($table, $kv){
        //table: in welke tabel gaan we de insert uitvoeren
        //kv: array K/V, als key het veld gaat meegeven en als value de waarde van het veld

        //1e oef. select all (*) van tabel met 1 argument...
        $sql_select = 'SELECT * FROM' .$table;
        var_dump("Read-statement (CRUD): {$sql_select}");

        //Resultaat(result set) van de query opvragen via het databaseconnection
        $result_select = $this->_db->query($sql_select);
        if ($result_select) {
            //Alle rijen in één keer opvragen.
            if ($rows_select = $result_select->fetchAll()) {
                var_dump('Records in' . $table . ':', $rows_select);
            }
        }
    }

    public function delete($table, $id){
        //table: in welke tabel gaan we de delete uitvoeren
        //id: welke record gaan we verwijderen in de tabel...
    }

    public function update($table, $kv){
        //table: in welke tabel gaan we de udate uitvoeren
        //kv: array K/V, key het veld gaat meegeven en als value de waarde van het veld
    }
}

$db = new Database();
$db->connect();
echo $db->select('employees');
//result...
//
$db->disconnect();