<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!--FAVICON-->

    <title>Portfolio Senne Vanfleteren</title>

    <link rel="stylesheet" href="<?php bloginfo('template_url');?>/css/normalize.css">
    <link rel="stylesheet" href="<?php bloginfo('template_url');?>/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php bloginfo('template_url');?>/css/style.css">
    <link href='https://fonts.googleapis.com/css?family=Lato:400,300,700' rel='stylesheet' type='text/css'>
    <script src="<?php bloginfo('template_url');?>/js/respond.js"></script>
    <script src="<?php bloginfo('template_url');?>/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>

    <?php wp_head(); ?>
</head>
<body>

<div class="navbar navbar-inverse navbar-fixed-top width-navigation">
    <div class="container">
        <a class="navbar-brand">
            <img src="<?php bloginfo('template_url');?>/img/logo.png" height="40px"/>
        </a>
        <button class="navbar-toggle navigation-button" data-toggle="collapse" data-target=".navHeader-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <div class="collapse navbar-collapse navHeader-collapse navigation">
            <?php
            wp_nav_menu( array(
                    'menu'              => 'primary',
                    'theme_location'    => 'primary',
                    'depth'             => 2,
                    'menu_class'        => 'nav navbar-nav navbar-right',
                    'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
                    'walker'            => new wp_bootstrap_navwalker())
            );
            ?>
        </div>
    </div>
</div>
